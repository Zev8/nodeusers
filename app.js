const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const bodyParser = require("body-parser");

const con = mongoose.connect("mongodb://5.189.177.98:37017/KalaitzisUsers", { useNewUrlParser: true });

// initialize express aplication
const app=express();

app.listen(3000);

//Define database models
global.User=require("./app/models/Users");

//middlewares
app.use(cors());
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());


app.get("/",(req,res)=>{
    res.send("Hello world");
}); 

app.use("/users",require("./app/routes/users"));

