const userList=async (req, res)=>{
    try{
   const users= await User.find().exec();
   res.json(users);
    }catch(e){
        console.log(e);
        res.send("error error error");
    }
};

const usersCreate=(req, res)=>{
    const fist_name = req.body.fist_name;
    const last_name = req.body.last_name;
    const email = req.body.email;
    const age = req.body.age;

    const u= new User({
        fist_name: fist_name,
        last_name: last_name,
        email: email,
        age: age
    });

    u.save();

    res.send("USERS CREATED");

};

module.exports={
    userList,
    usersCreate
};